#EVOLUTION#
An evolution programm that uses neural networks and machine learning. Creatures learn to eat.

#How to use#

just launch the main file. The program should then start. You can click on the gray moving squares to see what it sees.
green squares are food. If creature's eye sees food, then the input neuron that corresponds to it should turn to 1.
You can also add time or pause it. To see if they have learned anything, you can open a plot, which shows the history of creatures scores.
