from random import randint
from math import e, atan2, sqrt, asin, pi, cos, sin, radians
import pygame
import time
global midC
midC = 8 #number of middle layer neurons


class loom:

    def __init__(self,loc,xvel, yvel, neur):
        """

        :param loc: location of the animal
        :param xvel: the speed at which the animal is moving towards x axis
        :param yvel: the speed at which the animal is moving towards y axis
        :param vSpread: eye view angle
        :param neur: neurons that animal has
        :param foodInVision: These show if food is in vision or not (1 per eye)
        :param foodInRange: Shows how much food there is near the animal
        :return: returns score
        """
        self.loc = loc
        self.com_nr = randint(1,3)
        self.xvel = xvel
        self.yvel = yvel
        self.vel = 0
        self.vSpread = radians(50)

        self.foodInRange = 0
        #list contains date of each eye color, and direction relative to moving direction
        self.eyes = [[(0,255,0),radians(20)],[(0,0,255),radians(-20)],[(0,255,255),radians(60)],[(255,255,0),radians(-60)]]
        self.vision = []
        for i in range(len(self.eyes)):
            self.vision.append(-1)
        if self.xvel == 0:
            self.alpha = atan2(self.yvel,self.xvel+0.01)
        else:
            self.alpha = atan2(self.yvel,self.xvel)
        self.maxVisionRange = 100
        self.hunger = 3
        self.score = 0
        self.output = 0
        self.neuron = neurons(neur)

    def __repr__(self):
        return repr((self.score))

    def drawEye(self, pind, color, deg):
        alphaDeg = self.alpha + deg
        pygame.draw.line(pind, color, (cos(alphaDeg + self.vSpread / 2) * self.maxVisionRange + self.loc[0],
                                       sin(alphaDeg + self.vSpread / 2) * self.maxVisionRange + self.loc[1]), self.loc)
        pygame.draw.line(pind, color, (cos(alphaDeg - self.vSpread / 2) * self.maxVisionRange + self.loc[0],
                                       sin(alphaDeg - self.vSpread / 2) * self.maxVisionRange + self.loc[1]), self.loc)

    def update_vision(self,food_sources):
        #checking vision field

        self.foodInRange = 0
        for eye in self.eyes:
            for food in (food_sources):

                if self.maxVisionRange > sqrt((self.loc[0]-food[0])**2+(self.loc[1]-food[1])**2):
                    a = atan2(food[1] - self.loc[1], food[0] - self.loc[0])
                    if eye == self.eyes[0]:
                        self.foodInRange += 1
                    alpha2 = self.alpha
                    #fixes the angle. used to be from (-180) to (+180)
                    if a < 0:
                        a = 2*pi + a

                    if alpha2 < 0:
                        alpha2 = 2*pi + alpha2

                    alphaDeg = alpha2 + eye[1]
                    if (a < alphaDeg + self.vSpread / 2 and a > alphaDeg - self.vSpread / 2) or \
                        (a + 2* pi < alphaDeg + self.vSpread / 2 and a + 2* pi > alphaDeg - self.vSpread / 2) or \
                        (a - 2 * pi < alphaDeg + self.vSpread / 2 and a - 2 * pi > alphaDeg - self.vSpread / 2):
                        self.vision[self.eyes.index(eye)] = 1
                        break
                    else:
                        self.vision[self.eyes.index(eye)] = -1



        self.neuron.brain(self)
        self.output = self.neuron.outputs()
        self.alpha += self.output[0]
        self.vel = self.output[1]*20
        #self.alpha = atan2(self.yvel,self.xvel) #animal moving direction
        self.loc = (self.loc[0]+self.vel*cos(self.alpha), self.loc[1]+self.vel*sin(self.alpha))
        #if counter % 75 == 0:
        #    self.hunger += -1


class neurons:

    def __init__(self,n):
        self.sum = 0

        if n == 0:
            self.weighs1 = []
            self.weighs2 = []
            w = []
            v = []
            numberOfNeurons = 5

            #generating weights for layer 1 for the first time
            for i in range(numberOfNeurons*midC):
                w.append(randint(-12,12)/100)
                if (i+1) % numberOfNeurons == 0:
                    self.weighs1.append(w)
                    w = []

            #generating weights for layer 2 for the first time
            for i in range(midC*2):
                v.append(randint(-12,12)/100)
                if (i+1) % midC == 0:
                    self.weighs2.append(v)
                    v = []

        else:

            self.weighs1 = n[0]
            self.weighs2 = n[1]

    def brain(self, creature):
        #inputs: )
        self.b = [creature.foodInRange]
        for i in creature.vision:
            self.b.append(i)

    def functionG(self, neuronValue):
        return 1/(1+e**-neuronValue)

    def midLayer(self):
        self.mid = []
        for j in self.weighs1:
            a = 0
            for i in range (len(self.b)):
                a+= self.b[i] * j[i]
            a = self.functionG(a)
            self.mid.append(a)


    def outputs(self):
        self.midLayer()
        outp = []

        for j in self.weighs2:
            a = 0
            for i in range (midC):
                a+= self.mid[i] * j[i]
            outp.append(a)
        #print(outp)
        return outp






