import random
import classes

def evolve(creatures):
    creatures = sorted(creatures, key=lambda loom: loom.score, reverse=True)
    creatures = creatures[:10]
    nCreatures = creatures
    print(creatures)
    #nulling previous best creatures
    for i in creatures:
        i.loc = ((random.randint(10,1190),random.randint(10,790)))
        i.score = 0
    #generating new weighs (evolving)
    for i in range(int(len(creatures)/2)):
        creature1 = creatures[i*2]
        creature2 = creatures[i*2+1]

        for j in range(3): #number of children per pair
            nWeighs1 = [] #weighs, that connect input neurons with middle leyer neurons
            nWeighs2 = [] #weighs, that connect middle layer neurons with outputs
            nWeighs3 = [] #weighs, that connect all input neurons with middle layer neuron
            nWeighs4 = [] #weighs, that connect middle layer neurons with one output
            nWeigh = [] #nWeighs1 and nWeighs2 together

            for w in range(len(creature1.neuron.weighs1)):
                for n in range(len(creature1.neuron.weighs1[w])):
                    a = random.random()
                    #random mutations
                    k = 0
                    if random.random() < 0.1:
                        k = random.randint(-100,100)/100

                    if a < 0.4:
                        nWeighs3.append(creature1.neuron.weighs1[w][n]+k)
                    elif a >= 0.4 and a <= 0.6:
                        nWeighs3.append((creature1.neuron.weighs1[w][n]+creature2.neuron.weighs1[w][n])/2+k)
                    else:
                        nWeighs3.append(creature2.neuron.weighs1[w][n]+k)
                nWeighs1.append(nWeighs3)
                nWeighs3 = []

            for w in range(len(creature1.neuron.weighs2)):
                for n in range(len(creature1.neuron.weighs2[w])):
                    a = random.random()
                    #random mutations
                    k = 0
                    if random.random() < 0.1:
                        k = random.randint(-100,100)/100

                    if a < 0.4:
                        nWeighs4.append(creature1.neuron.weighs2[w][n])
                    elif a >= 0.4 and a <= 0.6:
                        nWeighs4.append((creature1.neuron.weighs2[w][n]+creature2.neuron.weighs2[w][n])/2)
                    else:
                        nWeighs4.append(creature2.neuron.weighs2[w][n])
                nWeighs2.append(nWeighs4)
                nWeighs4 = []
            nWeigh.append(nWeighs1)
            nWeigh.append(nWeighs2)
            nCreatures.append(classes.loom((random.randint(10,1190),random.randint(10,790)),0,0,nWeigh))
    return nCreatures
