import pygame
import classes
import random
from math import sqrt, cos, sin, pi
import numpy as np
import matplotlib.pyplot as plt
import button
import evolve
from time import sleep

pygame.init()
screenSize = (1600,800)
pind = pygame.display.set_mode( screenSize )
pygame.display.set_caption("Neurons")
pind.fill( (0,0,0) )
pygame.display.flip()
creatures = []
foodSources = []
foodAmount = 100
genTime = 300 #shows how many ticks are requied for next gen
avgScore2 = []
avgScore3 = []
gen = 0
selected = 0
foodSize = 10
creatureSize = 10
fontobject=pygame.font.SysFont('Arial', 12)
fontobject2=pygame.font.SysFont('Arial', 18)
showPlot = False
pause = False
pauseDraw = False
#First time generating stuff
for i in range(20):
    creatures.append(classes.loom((random.randint(10,screenSize[0]-410),random.randint(10,screenSize[1]-10)),0,0,0))
for i in range(foodAmount):
    foodSources.append((random.randint(10,screenSize[0]-410),random.randint(10,screenSize[1]-10)))

counter = 0


while True:
    event = pygame.event.poll()

    #drawing

    if pauseDraw == False:
        pind.fill( (255,255,255) )
        for i in creatures:
            if i != selected:
                pygame.draw.rect(pind,(100,100,100), (i.loc[0]-creatureSize/2,i.loc[1]-creatureSize/2,creatureSize,creatureSize))
                #temp
                #x = (cos(i.alpha)*i.maxVisionRange,sin(i.alpha)*i.maxVisionRange)
                #pygame.draw.line(pind,(0,255,0),i.loc,x)
            else:
                #drawing the selected creature
                alpha2 = i.alpha

                if alpha2 < 0:
                    alpha2 = 2*pi + alpha2
                pygame.draw.rect(pind,(0,100,100), (i.loc[0]-creatureSize/2,i.loc[1]-creatureSize/2,creatureSize,creatureSize))
                #moving direction (red)
                pygame.draw.line(pind,(255,0,0),(cos(i.alpha)*i.maxVisionRange + i.loc[0],sin(i.alpha)*i.maxVisionRange + i.loc[1]),i.loc)
                #drawing eyes
                for eye in i.eyes:
                    i.drawEye(pind,eye[0],eye[1])


        for i in foodSources:
            pygame.draw.rect(pind,(0,200,0), (i[0]-foodSize/2,i[1]-foodSize/2,foodSize,foodSize))

        pygame.draw.rect(pind,(150,150,150), (screenSize[0]-400,0,400,screenSize[1]))
        #Visualizing neurons

        if selected != 0:
            #inpur layer
            for i in range(len(selected.neuron.b)):
                if i > 0:
                    pygame.draw.rect(pind,selected.eyes[i-3][0], (screenSize[0]-370,300+15*i,10,10))

                else:
                    pygame.draw.rect(pind, (255,255,255), (screenSize[0]-370, 300 + 15 * i, 10, 10))
                pind.blit(fontobject.render(str(selected.neuron.b[i]), 1, (0, 0, 0)), (screenSize[0]-380, 300 + 15 * i))
            #mid layer
            for i in range(len(selected.neuron.mid)):
                pygame.draw.rect(pind,(255,255,255), (screenSize[0]-270,300+15*i,10,10))
                pind.blit(fontobject.render(str(selected.neuron.mid[i])[:5], 1, (0, 0, 0)),(screenSize[0]-300, 300+15*i))

            #output layer
            for i in range(len(selected.output)):
                pygame.draw.rect(pind,(255,255,255), (screenSize[0]-170,300+15*i,10,10))
                pind.blit(fontobject.render(str(selected.output[i])[:5], 1, (0, 0, 0)),(screenSize[0]-200, 300+15*i))


        #Scores and other info
        pygame.draw.rect(pind,(255,255,255), (screenSize[0]-300,0,300,200))
        pind.blit(fontobject2.render("gen: "+ str(gen), 1, (0, 0, 0)),(screenSize[0]-290, 20))
        pind.blit(fontobject2.render("Time till next gen: " + str(genTime - counter), 1, (0, 0, 0)), (screenSize[0]-290, 40))
        if gen > 0:
            pind.blit(fontobject2.render("last gen avg score: "+ str(avgScore), 1, (0, 0, 0)),(screenSize[0]-290, 60))
            pind.blit(fontobject2.render("avarage of avarage score: "+ str(sum(avgScore2)/len(avgScore2)), 1, (0, 0, 0)),(screenSize[0]-290, 80))

            if selected != 0:
                pind.blit(fontobject2.render("Selected creature stats:", 1, (0, 0, 0)),(screenSize[0]-290, 100))
                pind.blit(fontobject2.render("Score: " + str(selected.score), 1, (0, 0, 0)),(screenSize[0]-290, 120))
                pind.blit(fontobject2.render("Hunger: " + str(selected.hunger), 1, (0, 0, 0)),(screenSize[0]-290, 140))

        #plot enable button
        pygame.draw.rect(pind,(150,150,150), (screenSize[0]-30,120,30,30))
        pygame.draw.rect(pind,(255,255,255), (screenSize[0]-26,124,22,22))
        pind.blit(fontobject2.render("Show plot after this gen: ", 1, (0, 0, 0)),(screenSize[0]-190, 125))

        #drawing addTimeBtn
        button.Button(pind,screenSize[0]-110,220,90,40,"Add time", fontobject2)

        #drawing pauseBtn
        button.Button(pind,screenSize[0]-110,280,90,40,"Pause", fontobject2)

        #drawing pauseDrawBtn
        button.Button(pind,screenSize[0]-110,340,90,40,"Pause draw", fontobject2)

        if showPlot == True:
            pygame.draw.rect(pind,(0,255,0), (1574,124,22,22))



        pygame.display.flip()


    #sleep(0.02)
    #calculation part
    if pause == False:
        counter += 1
        for i in creatures:
            i.update_vision(foodSources)
            for j in foodSources:
                if round(i.loc[0]) - foodSize/2 < j[0] and i.loc[0] + foodSize/2 > j[0] and i.loc[1] + foodSize/2 > j[1] and i.loc[1] - foodSize/2 < j[1]:
                    i.score += 5
                    i.hunger += 2
                    foodSources.remove(j)
            #if (i.loc[0]>48 or i.loc[0] <0 or i.loc[1]> 25 or i.loc[1] < 0) and counter % 10 == 0:
            #    i.score -= 1
            #SELECTING
            if event.type == pygame.MOUSEBUTTONDOWN and 20 > sqrt((pygame.mouse.get_pos()[0]-i.loc[0])**2+(pygame.mouse.get_pos()[1]-i.loc[1])**2):
                selected = i
    else:
        for i in creatures:
            if event.type == pygame.MOUSEBUTTONDOWN and 20 > sqrt((pygame.mouse.get_pos()[0] - i.loc[0]) ** 2 + (pygame.mouse.get_pos()[1] - i.loc[1]) ** 2):
                selected = i

    #button logic part
    if event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pos()[0] > 1574 and pygame.mouse.get_pos()[0] < 1596 and pygame.mouse.get_pos()[1] > 124 and pygame.mouse.get_pos()[1] < 146 and showPlot == False:
        showPlot = True
    elif event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pos()[0] > 1574 and pygame.mouse.get_pos()[0] < 1596 and pygame.mouse.get_pos()[1] > 124 and pygame.mouse.get_pos()[1] < 146 and showPlot == True:
        showPlot = False
    #add time button
    elif event.type == pygame.MOUSEBUTTONDOWN and \
        pygame.mouse.get_pos()[0] > 1510 and\
        pygame.mouse.get_pos()[0] < 1590 and\
        pygame.mouse.get_pos()[1] > 220 and\
        pygame.mouse.get_pos()[1] < 260:

        counter-= 300

    #pause button
    elif event.type == pygame.MOUSEBUTTONDOWN and \
        pygame.mouse.get_pos()[0] > 1510 and\
        pygame.mouse.get_pos()[0] < 1590 and\
        pygame.mouse.get_pos()[1] > 280 and\
        pygame.mouse.get_pos()[1] < 320:

        if pause == True:
            pause = False
        else:
            pause = True
    #pause drawing button
    elif event.type == pygame.MOUSEBUTTONDOWN and \
        pygame.mouse.get_pos()[0] > 1510 and\
        pygame.mouse.get_pos()[0] < 1590 and\
        pygame.mouse.get_pos()[1] > 340 and\
        pygame.mouse.get_pos()[1] < 380:

        if pauseDraw == True:
            pauseDraw = False
        else:
            pauseDraw = True





    ##### FOR NEXT GENERATION
    if counter == genTime:
        #finding the closest remaining food distance from creatures.

        for i in creatures:
            if len(foodSources)> 0:
                foodDis = []
                for food in foodSources:
                    foodDis.append(sqrt((i.loc[0]-food[0])**2+(i.loc[1]-food[1])**2))
                if 3 - min(foodDis)/100 > 0:
                    i.score += 1 - min(foodDis)/100
            #if i.hunger == 3:
            #    i.score += 8
            #elif i.hunger == 2 or i.hunger == 4:
            #    i.score += 4
            #elif i.hunger > 6:
            #   creatures.remove(i)
        print("END OF GEN",gen)
        gen+=1
        counter = 0
        selected = 0
        foodSources = []
        avgScore = 0
        for e in range(foodAmount):
            foodSources.append((random.randint(10,1190),random.randint(10,790)))
        for i in creatures:
            avgScore+= i.score
        avgScore = avgScore/len(creatures)
        avgScore2.append(avgScore)
        avgScore3.append(sum(avgScore2)/len(avgScore2))
        print("avarage score",avgScore)
        print("avarage of avarage score",sum(avgScore2)/len(avgScore2))

        #evolving creatures
        creatures = evolve.evolve(creatures)
        for i in range(10):
            creatures.append(
                classes.loom((random.randint(10, screenSize[0] - 410), random.randint(10, screenSize[1] - 10)), 0, 0,
                             0))

        creatures.append(classes.loom((random.randint(10, screenSize[0] - 410), random.randint(10, screenSize[1] - 10)), 0, 0, 0))

        #Plots
        if showPlot == True:
            # Create a figure of size 8x6 inches, 80 dots per inch
            plt.figure(figsize=(12, 9), dpi=80)

            # Create a new subplot from a grid of 1x1
            plt.subplot(1, 1, 1)

            X = np.linspace(0, len(avgScore2), len(avgScore2), endpoint=True)

            C = avgScore2
            C2 = avgScore3

            # Plot cosine with a blue continuous line of width 1 (pixels)
            plt.plot(X, C, color="blue", linewidth=1.0, linestyle="-")
            plt.plot(X, C2, color="red", linewidth=1.0, linestyle="-")
            # Set x limits
            plt.xlim(0, len(avgScore2))

            # Set x ticks
            plt.xticks(np.linspace(0, len(avgScore2), 9, endpoint=True))

            # Set y limits
            plt.ylim(0, 1.0)

            # Set y ticks
            plt.yticks(np.linspace(min(avgScore2), max(avgScore2), 5, endpoint=True))

            # Save figure using 72 dots per inch
            # plt.savefig("exercice_2.png", dpi=72)

            # Show result on screen

            plt.show()
            showPlot = False


pygame.quit()
